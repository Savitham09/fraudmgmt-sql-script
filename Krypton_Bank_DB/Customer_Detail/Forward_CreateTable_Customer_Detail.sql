USE Krypton_Bank_DB;
CREATE TABLE IF NOT EXISTS Customer_Detail(
CUSTOMER_ID int PRIMARY KEY,
FIRST_NAME varchar(20),
MIDDLE_NAME varchar(20),
LAST_NAME varchar(20),
EMAIL varchar(30) UNIQUE,
PHONE_NUMBER BIGINT(10) UNSIGNED UNIQUE,
PAN_CARD varchar(10)UNIQUE,
GENDER ENUM('Male','Female','Other')NOT NULL,
ADDRESS varchar(50), 
PASSWORD varchar(30));

SELECT * FROM krypton_bank_db.Customer_Detail;


 