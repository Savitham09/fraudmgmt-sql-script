USE Krypton_Bank_DB;

CREATE TABLE IF NOT EXISTS Employee_Detail(
EMPLOYEE_ID int primary key,
NAME varchar(30),
EMAIL varchar(30)UNIQUE,
PHONE_NO BIGINT(10) UNSIGNED UNIQUE,
PASSWORD varchar(30),
EMPLOYEE_TYPE varchar(30),
GENDER ENUM('Male','Female','Other')NOT NULL,
NO_OF_CASE_HANDLING int)

SELECT * FROM krypton_bank_db.Employee_Detail;